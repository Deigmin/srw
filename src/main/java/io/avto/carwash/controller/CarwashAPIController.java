package io.avto.carwash.controller;

import io.avto.carwash.dao.CarwashJooqDao;
import io.avto.db.tables.pojos.Carwash;
import io.avto.db.tables.pojos.Device;
import io.avto.db.tables.pojos.Image;
import io.avto.device.dao.DeviceJooqDao;
import io.avto.image.dao.ImageJooqDao;
import io.avto.image.service.ImageStorageService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Created by denis on 05.06.2016.
 */
@Controller
public class CarwashAPIController {
    private static Logger logger = LoggerFactory.getLogger(CarwashAPIController.class);

    @Autowired
    private CarwashJooqDao carwashDao;

    @RequestMapping(value="/api/carwashes", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Carwash>> getCarwashList() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccessControlAllowOrigin("*");
        return new ResponseEntity<>(carwashDao.getAll(), headers, HttpStatus.OK);
    }

}
