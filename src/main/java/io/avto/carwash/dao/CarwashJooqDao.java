package io.avto.carwash.dao;

import io.avto.db.tables.pojos.Carwash;
import io.avto.db.tables.records.CarwashRecord;
import org.jooq.DSLContext;
import org.jooq.types.ULong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static io.avto.db.Tables.CARWASH;

/**
 * Created by denis on 11.08.2016.
 */
@Repository
public class CarwashJooqDao {
    @Autowired
    private DSLContext dslContext;

    public void insert(Carwash carwash) {
        CarwashRecord carwashRecord = dslContext.insertInto(CARWASH,
                CARWASH.ADDRESS, CARWASH.NAME,
                CARWASH.LATITUDE, CARWASH.LONGITUDE)
        .values(carwash.getAddress(), carwash.getName(),
                carwash.getLatitude(), carwash.getLongitude())
        .returning()
        .fetchOne();

        carwash.setId(carwashRecord.getId());
    }

    public Optional<Carwash> getById(long id) {
        return dslContext.select()
                .from(CARWASH)
                .where(CARWASH.ID.equal(ULong.valueOf(id)))
                .fetchOptionalInto(Carwash.class);
    }

    public List<Carwash> getAll() {
        return dslContext.select()
                .from(CARWASH)
                .fetch()
                .into(Carwash.class);
    }
}
