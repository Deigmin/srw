package io.avto.common;

import org.jooq.Configuration;
import org.jooq.ConnectionProvider;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.sql.DataSource;

/**
 * Created by denis on 11.08.2016.
 */
public class DatabaseConfiguration {
    @Bean
    public ConnectionProvider connectionProvider(DataSource dataSource) {
        return new DataSourceConnectionProvider
                (new TransactionAwareDataSourceProxy(dataSource));
    }

    @Bean
    public DefaultDSLContext dsl(Configuration configuration) {
        return new DefaultDSLContext(configuration);
    }

    public Configuration configuration(ConnectionProvider connectionProvider) {
        Configuration jooqConfiguration = new DefaultConfiguration()
                .derive(connectionProvider)
                .derive();

        return jooqConfiguration;
    }
}
