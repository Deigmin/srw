/**
 * This class is generated by jOOQ
 */
package io.avto.db.tables;


import io.avto.db.Avtoiobase;
import io.avto.db.Keys;
import io.avto.db.tables.records.CarwashRecord;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;
import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Carwash extends TableImpl<CarwashRecord> {

    private static final long serialVersionUID = 1452443282;

    /**
     * The reference instance of <code>avtoiobase.carwash</code>
     */
    public static final Carwash CARWASH = new Carwash();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<CarwashRecord> getRecordType() {
        return CarwashRecord.class;
    }

    /**
     * The column <code>avtoiobase.carwash.id</code>.
     */
    public final TableField<CarwashRecord, ULong> ID = createField("id", org.jooq.impl.SQLDataType.BIGINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>avtoiobase.carwash.name</code>.
     */
    public final TableField<CarwashRecord, String> NAME = createField("name", org.jooq.impl.SQLDataType.VARCHAR.length(200).nullable(false), this, "");

    /**
     * The column <code>avtoiobase.carwash.address</code>.
     */
    public final TableField<CarwashRecord, String> ADDRESS = createField("address", org.jooq.impl.SQLDataType.VARCHAR.length(500), this, "");

    /**
     * The column <code>avtoiobase.carwash.latitude</code>.
     */
    public final TableField<CarwashRecord, BigDecimal> LATITUDE = createField("latitude", org.jooq.impl.SQLDataType.DECIMAL.precision(9, 6), this, "");

    /**
     * The column <code>avtoiobase.carwash.longitude</code>.
     */
    public final TableField<CarwashRecord, BigDecimal> LONGITUDE = createField("longitude", org.jooq.impl.SQLDataType.DECIMAL.precision(9, 6), this, "");

    /**
     * Create a <code>avtoiobase.carwash</code> table reference
     */
    public Carwash() {
        this("carwash", null);
    }

    /**
     * Create an aliased <code>avtoiobase.carwash</code> table reference
     */
    public Carwash(String alias) {
        this(alias, CARWASH);
    }

    private Carwash(String alias, Table<CarwashRecord> aliased) {
        this(alias, aliased, null);
    }

    private Carwash(String alias, Table<CarwashRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Avtoiobase.AVTOIOBASE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<CarwashRecord, ULong> getIdentity() {
        return Keys.IDENTITY_CARWASH;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<CarwashRecord> getPrimaryKey() {
        return Keys.KEY_CARWASH_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<CarwashRecord>> getKeys() {
        return Arrays.<UniqueKey<CarwashRecord>>asList(Keys.KEY_CARWASH_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Carwash as(String alias) {
        return new Carwash(alias, this);
    }

    /**
     * Rename this table
     */
    public Carwash rename(String name) {
        return new Carwash(name, null);
    }
}
