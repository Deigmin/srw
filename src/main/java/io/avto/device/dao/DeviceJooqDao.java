package io.avto.device.dao;

import io.avto.db.tables.pojos.Device;
import io.avto.db.tables.records.DeviceRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import static io.avto.db.Tables.DEVICE;

/**
 * Created by denis on 11.08.2016.
 */
@Repository
public class DeviceJooqDao {
    @Autowired
    private DSLContext dslContext;

    public void insert(Device device) {
        DeviceRecord deviceRecord = dslContext.insertInto(DEVICE)
                .set(DEVICE.CARWASH_ID, device.getCarwashId())
                .set(DEVICE.CODE, device.getCode())
                .returning(DEVICE.ID)
                .fetchOne();

        device.setId(deviceRecord.getId());
    }

    public Optional<Device> getByCode(String deviceCode) {
        return dslContext.select()
                .from(DEVICE)
                .where(DEVICE.CODE.equal(deviceCode))
                .fetchOptionalInto(Device.class);
    }
}
