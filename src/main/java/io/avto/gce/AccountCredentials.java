package io.avto.gce;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.storage.StorageScopes;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;

/**
 * Created by denis on 01.06.2016.
 */
@Service
public class AccountCredentials {

    public Credential getCredential() throws IOException {
        return GoogleCredential.fromStream(
                AccountCredentials.class.getResourceAsStream("/properties/dev/gce.json"))
                .createScoped(Collections.singleton(StorageScopes.DEVSTORAGE_FULL_CONTROL));
    }

}
