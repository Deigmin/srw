package io.avto.gce;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.StorageObject;
import org.apache.http.client.CredentialsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by denis on 01.06.2016.
 */
@Service
public class StorageService {
    @Autowired
    private AccountCredentials accountCredentials;
    private Storage storage;

    @PostConstruct
    public void init() {
        try {
            HttpTransport httpTransport = new ApacheHttpTransport(ApacheHttpTransport.newDefaultHttpClient());
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            Credential credential = accountCredentials.getCredential();
            storage = new Storage.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName("image-upload/1.0").build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Storage getStorage() {
        return storage;
    }
}
