package io.avto.image.application;

import org.springframework.boot.SpringApplication;

/**
 * Created by denis on 05.06.2016.
 */
public class ImageAPIApplication {
    public static void main(String[] args) {
        SpringApplication.run(ImageAPIConfiguration.class, args);
    }
}
