package io.avto.image.application;

import io.avto.image.service.GceImageStorageService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan({"io.avto"})
@EnableAutoConfiguration
@EnableTransactionManagement
@PropertySource("classpath:/properties/global.properties")
@PropertySource("classpath:/properties/dev/datasource.properties")
public class ImageAPIConfiguration {

    @Bean
    public GceImageStorageService uploadService() {
        return new GceImageStorageService();
    }

}
