package io.avto.image.controller;

import io.avto.carwash.dao.CarwashJooqDao;
import io.avto.db.tables.pojos.Carwash;
import io.avto.db.tables.pojos.Device;
import io.avto.db.tables.pojos.Image;
import io.avto.device.dao.DeviceJooqDao;
import io.avto.image.dao.ImageJooqDao;
import io.avto.image.service.ImageStorageService;
import org.joda.time.DateTime;
import org.jooq.types.ULong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by denis on 05.06.2016.
 */
@Controller
public class ImageAPIController {
    private static Logger logger = LoggerFactory.getLogger(ImageAPIController.class);

    @Autowired
    ImageStorageService imageStorageService;
    @Autowired
    private ImageJooqDao imageDao;
    @Autowired
    private DeviceJooqDao deviceDao;
    @Autowired
    private CarwashJooqDao carwashDao;

    @RequestMapping(value="/api/images/devices/{deviceCode}/images", method= RequestMethod.POST)
    @ResponseBody
    public String upload(@RequestParam("file") MultipartFile file,
                            @PathVariable("deviceCode") String deviceCode) throws IOException {
        Optional<Device> device = deviceDao.getByCode(deviceCode);
        if (!device.isPresent()) {
            throw new IllegalArgumentException(deviceCode + " is not found");
        }

        if (file.isEmpty()) {
            throw new IllegalArgumentException("File is required");
        }

        DateTime now = new DateTime();
        String fileName = now.toString("HH-mm-ss-sss") + ".jpg";
        String folder = now.toString("yyyy-MM-dd");
        String path = folder + "/" + device.get().getId() + "/" + fileName;

        imageStorageService.uploadStreamAsJpgImage(
                new ByteArrayInputStream(file.getBytes()),
                path);
        logger.info("Uploaded {}", path);

        Image image = new Image()
                .setDeviceId(device.get().getId())
                .setCreatedDttm(new Timestamp(System.currentTimeMillis()))
                .setPath(path);

        imageDao.insert(image);

        return  "/api/images/devices/" + device.get().getId() + "/images/" + folder + "/" + fileName;
    }

    @RequestMapping(value="/api/images/devices/{deviceId}/images/{folder}/{fileName}", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<InputStreamResource> download(
            @PathVariable("deviceId") String deviceId,
            @PathVariable("fileName") String fileName,
            @PathVariable("folder") String folder) {

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        String path = folder + "/" + deviceId + "/" + fileName + ".jpg";
        logger.info("Downloading image {}", path);
        return new ResponseEntity<>(
                new InputStreamResource(imageStorageService.downloadImageAsStream(path)), headers, HttpStatus.OK);
    }

    @RequestMapping(value="/api/images/devices/{deviceCode}/images/latest", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<InputStreamResource> downloadLatest(
            @PathVariable("deviceCode") String deviceCode) {
        Optional<Device> device = deviceDao.getByCode(deviceCode);
        if (!device.isPresent()) {
            throw new IllegalArgumentException(deviceCode + " is not found");
        }

        Optional<Image> lastImage = imageDao.getLastForDeviceId(device.get().getId().longValue());

        if (!lastImage.isPresent()) {
            throw new IllegalArgumentException("No image for" + deviceCode);
        }

        return returnImage(lastImage);
    }

    private ResponseEntity<InputStreamResource> returnImage(Optional<Image> lastImage) {
        logger.info("Downloading image {}", lastImage.get().getPath());
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<>(
                new InputStreamResource(imageStorageService.downloadImageAsStream(
                        lastImage.get().getPath())), headers, HttpStatus.OK);
    }

    @RequestMapping(value="/api/images/carwashes/{id}/images/latest", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<InputStreamResource> downloadLatestForCarWash(
            @PathVariable("id") Long carwashId) {
        Optional<Carwash> carwash = carwashDao.getById(carwashId);
        if (!carwash.isPresent()) {
            throw new IllegalArgumentException(carwashId + " is not found");
        }

        Optional<Image> lastImage = imageDao.getLastForCarwashId(carwashId);

        if (!lastImage.isPresent()) {
            throw new IllegalArgumentException("No image for" + carwashId);
        }

        return returnImage(lastImage);
    }

}
