package io.avto.image.dao;

import io.avto.db.tables.pojos.Carwash;
import io.avto.db.tables.pojos.Image;
import io.avto.db.tables.records.CarwashRecord;
import io.avto.db.tables.records.ImageRecord;
import org.jooq.DSLContext;
import org.jooq.types.ULong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Optional;

import static io.avto.db.Tables.CARWASH;
import static io.avto.db.Tables.DEVICE;
import static io.avto.db.Tables.IMAGE;

/**
 * Created by denis on 11.08.2016.
 */
@Repository
public class ImageJooqDao {
    @Autowired
    private DSLContext dslContext;

    public void insert(Image image) {
        ImageRecord imageRecord = dslContext.insertInto(IMAGE)
                .set(IMAGE.PATH, image.getPath())
                .set(IMAGE.CREATED_DTTM, new Timestamp(System.currentTimeMillis()))
                .set(IMAGE.DEVICE_ID, image.getDeviceId())
                .returning(IMAGE.ID)
                .fetchOne();

        image.setId(imageRecord.getId());
    }

    public Optional<Image> getLastForDeviceId(long id) {
        return dslContext.select()
                .from(IMAGE)
                .where(IMAGE.DEVICE_ID.equal(ULong.valueOf(id)))
                .orderBy(IMAGE.CREATED_DTTM.desc())
                .limit(1)
                .fetchOptionalInto(Image.class);
    }

    public Optional<Image> getLastForCarwashId(long id) {
        return dslContext.select()
                .from(IMAGE)
                    .join(DEVICE)
                    .onKey()
                .where(DEVICE.CARWASH_ID.equal(ULong.valueOf(id)))
                .orderBy(IMAGE.CREATED_DTTM.desc())
                .limit(1)
                .fetchOptionalInto(Image.class);
    }
}
