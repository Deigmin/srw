package io.avto.image.service;

import com.google.api.client.http.InputStreamContent;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.StorageObject;
import io.avto.gce.StorageService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by denis on 01.06.2016.
 */
public class GceImageStorageService implements ImageStorageService {

    @Autowired
    StorageService storageService;
    private String bucket = "avtoio-dev-upload";

    @Override
    public StorageObject uploadStreamAsJpgImage(InputStream inputStream, String path) {
        InputStreamContent mediaContent = new InputStreamContent("image/jpeg", inputStream);
        try {
            Storage.Objects.Insert insertObject = storageService.getStorage()
                    .objects().insert(bucket, null, mediaContent)
                    .setName(path);
            insertObject.getMediaHttpUploader()
//                    .setDirectUploadEnabled(true)
                    .setDisableGZipContent(true);
            return insertObject.execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public InputStream downloadImageAsStream(String path) {
        try {
            Storage.Objects.Get getObject = storageService.getStorage()
                    .objects().get(bucket, path);
            getObject.getMediaHttpDownloader()
                    .setDirectDownloadEnabled(true);
            return getObject.executeMediaAsInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
