package io.avto.image.service;

import com.google.api.services.storage.model.StorageObject;

import java.io.InputStream;

/**
 * Created by denis on 01.06.2016.
 */
public interface ImageStorageService {
    StorageObject uploadStreamAsJpgImage(InputStream inputStream, String path);

    InputStream downloadImageAsStream(String path);
}
