CREATE DATABASE avtoiobase;
CREATE USER 'cloudsqlproxy'@'cloudsqlproxy~%' IDENTIFIED BY 'cloudsqlproxy';
CREATE USER 'app'@'%' IDENTIFIED BY 'app';
GRANT ALL ON avtoiobase.* TO 'app'@'%';
GRANT ALL ON avtoiobase.* TO 'cloudsqlproxy'@'%';
GRANT SELECT ON mysql.* TO 'app'@'%';