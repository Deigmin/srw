package io.avto.image.dao;

import io.avto.db.tables.pojos.Image;
import io.avto.image.application.ImageAPIConfiguration;
import org.jooq.types.ULong;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by denis on 17.08.2016.
 */
@SpringBootTest(classes = ImageAPIConfiguration.class)
@RunWith(SpringRunner.class)
public class ImageJooqDaoTest {
    @Autowired
    private ImageJooqDao dao;

    @Test
    public void savesAndLoadsCorrectly() {
        Image image = new Image()
                .setDeviceId(ULong.valueOf(1L))
                .setCreatedDttm(new Timestamp(System.currentTimeMillis()))
                .setPath("/test");
        dao.insert(image);
        assertThat(image.getId()).isNotNull();

        Optional<Image> image2 = dao.getLastForDeviceId(1L);
        assertThat(image2.isPresent()).isTrue();
        assertThat(image2.get().getId()).isEqualTo(image.getId());

        Optional<Image> image3 = dao.getLastForCarwashId(1L);
        assertThat(image3.isPresent()).isTrue();
        assertThat(image3.get().getId()).isEqualTo(image.getId());
    }
}
