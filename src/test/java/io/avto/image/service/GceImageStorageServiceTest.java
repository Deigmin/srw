package io.avto.image.service;

import io.avto.image.application.ImageAPIConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by denis on 01.06.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ImageAPIConfiguration.class)
public class GceImageStorageServiceTest {
    @Autowired
    ImageStorageService imageStorageService;

    @Test
    public void uploadsFileToGce() {
        imageStorageService.uploadStreamAsJpgImage(this.getClass().
                getClassLoader().getResourceAsStream("image.jpg"), "test.jpg");
    }

}
